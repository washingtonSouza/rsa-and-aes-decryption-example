﻿using Encryption;
using System;
using System.IO;
using System.Security.Cryptography;

namespace CPFDecryptExample
{
    class Program
    {
        public static void Main(string[] args)
        {
            string privKey = @"MIIEpQIBAAKCAQEAvmHH4+nIOLEalQpRwJyeel05WpphIQzUJcOO0380qQXzasfM
                               t8912gTQSgmHzkmQ9l2t4J/dvNiH2Pih4HwNPolup0yV0fRV15HJ3Wq+0M04/BXm
                               IPtCgD8UgKqZ7LG1VeNMVuun3Rw/X52S5JqXTUcgAtOKlAKd07Ua+GCdURXFaJzs
                               5OnhN1OhKbQHq17tH0toWSV7sXCMeKqBZCEKDt5LQ3Z+ZwCJrPGHHvSw3wNAfdgt
                               Jc3+29fsHApj4ZtmvbjZEstLvPjYdtsA9L/zt5Ou77w5w8tU8uRKfGkQgeegrVyj
                               tWnaFho2siGMwV1PBuN6s56fFcoX67W/UfCC4QIDAQABAoIBAQCfWSx8AUNK4EHF
                               mZuPwgeLl9fXk/vUM+6lpSc4svIavxZcu9sHxqd30iucrJl0+nJRI1vT9TsdQ4eA
                               /apQQMS9IT7y3NFRqFbpVedGYoBQOFWH+bRiyquAuEuq3iBz0tJajyNoZVpdYbVR
                               jIxUt2gIkI2kj/cfp4j+mEl4tzU1lbeAWWI6OAJ3bKjejgJK9pL7PJwwywB9LtVG
                               ANHTZRsnkZu0wUnWHsod1u6uNsMAz74agIdW51pVUPIHUYET+igYNSAM14briVhA
                               cegOgBFQRlxKVD5DXKfmAbsRZXO0Fk2CcT7ThUbP6MLa3xCUv4N3K+el6EmyGENa
                               2L2EVDCBAoGBAOYwLm1FpHSh/5plakdzqpAbWkH9guFsf/Sqpu6LNFyQMJveiZ3W
                               ha4mhxVDziMlyauY/miEBKwQej6i58VmH7Q/esUxOm1El+DDs8aQhfiKqA886c3u
                               E2oEmc49cLVY1hGcwCOMMFJk7ghM/ZKI90GKMBnmSP6FOQIywxuvAsLpAoGBANO6
                               6jwj4FgxAwtfPEv6eGcf9tPXCsDWAoyZpojP/Ndml/DLgls5NvPYn6U3CSo3l7pj
                               D23ium4ci8MSDd43Z9DV+aP6PyIp74scTs+JFdnCNTM3kzgGPazsP6Jlxyby7DwO
                               KY61fl2jMUO+TEr/GPtgT1ymPpo3RRKI0AcL5hU5AoGBAMyh5pQDxlhdL5J9N8b/
                               X5CukEV77h8iSnoiPzZvEAR9tojKg8KKU9rmq03PMnDyhd8Dqp8VReYE2/fU8H/+
                               8o5VpHmq8sryoKYStD4AeujhstehCiSk0iN8pZK/8fN1XxDXLcQsO0T0Ltu4PfBH
                               zK85/tr3asrBelWm9OYmZikJAoGBANGZ4jQzF+52ZpslZl2Y0QTV4dWItn573f4u
                               RWx+6cCbMaMQ9DU2iK4RLsmxmAMREcVUN3wCzagf3kP4ZX4svzs/+GhaXZGGjjra
                               hTJydQ0jcuMHC1iU5Q2AuuYbd3GZXj99japXBYfQ+/4iLwi3TJXdm/MOKD8mo9xb
                               UAUJxHYRAoGAFGpNCp1MsREJp2qzJ4GGmrOSSVJbPZCnsGN7R1t3vo+32/3nFQnE
                               UHT1uF3Ykbxst9zYKumejgEwa5ClFSecQy1jmKQ6uX64SmEdWzpNZ0GH5cvfCfki
                               18px4pZQsyckB3U1fW4zfqIF1BiQdZ7A6vEWiacnDwWL0Nt+WOCATPk=";

            string key = "fUx16JyZfG9tW8IESR3+S0G0IfurFWAzho/r6IRciHcYlSonxNJWZTprsGxLjxTNzX4Cu7brWNQmaafBRtnAkOjxsjf0tM+x5XAfhGrpAtdXEE9YGwBdGYaxhp+NYNn4yVbLbVtnymF3gbLNbTW2VzHGNhDPK3CcA6dWiXTdG8yAIKu1QaVh2hpLkhPOqJE0BkCkD0Ievslt0dK4jJ4Kc6bPOQukP9Sl6nEOQxKPEoqQEqCR/lg3IqcGEopx7Frmi9ad/bLWhSyIgWAiLhAwXTsdE/xmJuz5CAYlV6DM0BfMcuoMGZ3qx19Tu+4NqnyEu4WdktPDhy9I2F69yi8O/A==";
            byte[] EnKeyA = Convert.FromBase64String(key);

            string cipheredtext = "kuCnVkDl3HzOXMCUYUK80SlfbHwsk/AGzBvukfsLeeByQkeAGpn6UywF3MQ=";
            byte[] EnCPF = Convert.FromBase64String(cipheredtext);

            byte[] keyA = null;

            RSAParameters parameters = CreateRsaProviderFromPrivateKey(privKey);
            using (RSA rsa = new RSACng())
            {
                rsa.ImportParameters(parameters);
                keyA = rsa.Decrypt(EnKeyA, RSAEncryptionPadding.OaepSHA256);
            }

            //Install BouncyCastle libray (available on NuGet) in order to use the code bellow
            byte[] bytesCPF = AESGCM.SimpleDecrypt(EnCPF, keyA);

            string decodedCPF = System.Text.Encoding.UTF8.GetString(bytesCPF);

            Console.Write("Decoded CPF:" + decodedCPF);

            Console.ReadKey();
        }

        private static RSAParameters CreateRsaProviderFromPrivateKey(string privateKey)
        {
            var privateKeyBits = System.Convert.FromBase64String(privateKey);

            var RSA = new RSACryptoServiceProvider();
            var RSAparams = new RSAParameters();

            using (BinaryReader binr = new BinaryReader(new MemoryStream(privateKeyBits)))
            {
                byte bt = 0;
                ushort twobytes = 0;
                twobytes = binr.ReadUInt16();
                if (twobytes == 0x8130)
                    binr.ReadByte();
                else if (twobytes == 0x8230)
                    binr.ReadInt16();
                else
                    throw new Exception("Unexpected value read binr.ReadUInt16()");

                twobytes = binr.ReadUInt16();
                if (twobytes != 0x0102)
                    throw new Exception("Unexpected version");

                bt = binr.ReadByte();
                if (bt != 0x00)
                    throw new Exception("Unexpected value read binr.ReadByte()");

                RSAparams.Modulus = binr.ReadBytes(GetIntegerSize(binr));
                RSAparams.Exponent = binr.ReadBytes(GetIntegerSize(binr));
                RSAparams.D = binr.ReadBytes(GetIntegerSize(binr));
                RSAparams.P = binr.ReadBytes(GetIntegerSize(binr));
                RSAparams.Q = binr.ReadBytes(GetIntegerSize(binr));
                RSAparams.DP = binr.ReadBytes(GetIntegerSize(binr));
                RSAparams.DQ = binr.ReadBytes(GetIntegerSize(binr));
                RSAparams.InverseQ = binr.ReadBytes(GetIntegerSize(binr));
            }

            RSA.ImportParameters(RSAparams);
            return RSAparams;
        }

        private static int GetIntegerSize(BinaryReader binr)
        {
            byte bt = 0;
            byte lowbyte = 0x00;
            byte highbyte = 0x00;
            int count = 0;
            bt = binr.ReadByte();
            if (bt != 0x02)
                return 0;
            bt = binr.ReadByte();

            if (bt == 0x81)
                count = binr.ReadByte();
            else
                if (bt == 0x82)
            {
                highbyte = binr.ReadByte();
                lowbyte = binr.ReadByte();
                byte[] modint = { lowbyte, highbyte, 0x00, 0x00 };
                count = BitConverter.ToInt32(modint, 0);
            }
            else
            {
                count = bt;
            }

            while (binr.ReadByte() == 0x00)
            {
                count -= 1;
            }
            binr.BaseStream.Seek(-1, SeekOrigin.Current);
            return count;
        }
    }
}
